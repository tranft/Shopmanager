package de.awacademy.Shopmanager;

import de.awacademy.Shopmanager.link.Link;
import de.awacademy.Shopmanager.link.LinkRepository;
import de.awacademy.Shopmanager.user.User;
import de.awacademy.Shopmanager.user.UserRepository;
import de.awacademy.Shopmanager.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Random;

@Component
public class SetupComponent {

    private final LinkRepository linkRepository;
    private final UserService userService;
    private final UserRepository userRepository;

    @Autowired
    public SetupComponent(LinkRepository linkRepository, UserService userService, UserRepository userRepository) {
        this.linkRepository = linkRepository;
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @EventListener
    @Transactional
    public void handleApplicationReady(ApplicationReadyEvent event) {
        if (!userService.usernameExists("test")) {
            userService.register("test", "123456");
        }
        if (!userService.usernameExists("admin")) {
            userService.registerAdmin("admin", "123456");
        }
        if (linkRepository.count() == 0) {
            User user = userRepository.findByUsername("test").get();
            Random r = new Random();
            linkRepository.save(new Link("Shop 1", "https://www.awacademy.de", r.nextInt(100), user));
            linkRepository.save(new Link("Shop 2", "https://de.wikipedia.org/", r.nextInt(100), user));
            linkRepository.save(new Link("Shop 3", "https://www.jetbrains.com/idea/", r.nextInt(100), user));
            linkRepository.save(new Link("Shop 4", "https://canvas.academy.se/", r.nextInt(100), user));
            linkRepository.save(new Link("Shop 5", "https://www.thymeleaf.org/", r.nextInt(100), user));
            linkRepository.save(new Link("Shop 6", "https://angular.io/", r.nextInt(100), user));
            linkRepository.save(new Link("Shop 7", "https://developer.mozilla.org", r.nextInt(100),user));
            linkRepository.save(new Link("Shop 8", "https://bulma.io/", r.nextInt(100),user));
            linkRepository.save(new Link("Shop 9", "https://spring.io/", r.nextInt(100), user));
            linkRepository.save(new Link("Shop 10", "https://www.hackerrank.com/", r.nextInt(100), user));
            linkRepository.save(new Link("Shop 11", "https://codesignal.com/", r.nextInt(100), user));
            linkRepository.save(new Link("Shop 12", "https://spring.io/guides/gs/securing-web/", 100, user));
        }
    }
}
