package de.awacademy.Shopmanager;


import de.awacademy.Shopmanager.link.Link;
import de.awacademy.Shopmanager.link.LinkDTO;
import de.awacademy.Shopmanager.link.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
public class HomeController {

  private final LinkRepository linkRepository;

  @Autowired
  public HomeController(LinkRepository linkRepository) {
    this.linkRepository = linkRepository;
  }

  @GetMapping("/api")
  public String home(Model model, @RequestParam(defaultValue = "false") boolean linkSubmitted,
                     @RequestParam(defaultValue = "false") boolean voted,
                     @RequestParam(defaultValue = "false") boolean registered,
                     @RequestParam(defaultValue = "false") boolean deleted) {
    model.addAttribute("links", linkRepository.findAllByOrderByPointsDesc());
    model.addAttribute("linkSubmitted", linkSubmitted);
    model.addAttribute("voted", voted);
    model.addAttribute("registered", registered);
    model.addAttribute("deleted", deleted);
    return "home";
  }

  @GetMapping("/api/shops")
  public List<LinkDTO> lesen() {
    List<LinkDTO> response = new LinkedList<>();

    for (Link link : linkRepository.findAll()) {
      response.add(new LinkDTO(link.getTitle(), link.getUrl()));
    }

    return response;
  }
}
