package de.awacademy.Shopmanager.user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }
    public boolean usernameExists(String username) {
        return userRepository.existsByUsernameIgnoreCase(username);
    }

    public User register(String username, String password) {
        String encodedPassword = passwordEncoder.encode(password);
        return userRepository.save(new User(username, encodedPassword));
    }
    public User registerAdmin(String username, String password) {
        String encodedPassword = passwordEncoder.encode(password);

        User newUser = new User(username, encodedPassword);
        newUser.setAdmin(true);
        userRepository.save(newUser);
        return newUser;
    }
}
