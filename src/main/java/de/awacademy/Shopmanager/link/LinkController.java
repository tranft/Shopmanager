package de.awacademy.Shopmanager.link;

import de.awacademy.Shopmanager.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@RestController
public class LinkController {

    private final LinkRepository linkRepository;

    @Autowired
    public LinkController(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    @GetMapping("/api/submit")
    public String submit(Model model) {
        model.addAttribute("link", new LinkDTO("", ""));
        return "submit";
    }

    @PostMapping("/api/submit")
    public String submit(@Valid @ModelAttribute("link") LinkDTO linkDTO, BindingResult bindingResult,
                         @ModelAttribute("sessionUser") User sessionUser,
                         RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "submit";
        }

        Link link = new Link(linkDTO.getTitle(), linkDTO.getUrl(), sessionUser);
        linkRepository.save(link);

        redirectAttributes.addAttribute("linkSubmitted", true);
        return "redirect:/api";
    }

    @PostMapping("/api/vote")
    public String upvote(VoteDTO voteDTO, RedirectAttributes redirectAttributes) {
        Link link = linkRepository.findById(voteDTO.getLinkId()).orElseThrow();

        if (voteDTO.isUp()) {
            link.upvote();
        }
        else {
            link.downvote();
        }

        linkRepository.save(link);

        redirectAttributes.addAttribute("voted", true);
        return "redirect:/api";
    }

    @PostMapping("/api/delete/{linkId}")
    public String delete(@PathVariable long linkId, RedirectAttributes redirectAttributes) {
        linkRepository.deleteById(linkId);
        redirectAttributes.addAttribute("deleted", true);
        return "redirect:/api";
    }
}
