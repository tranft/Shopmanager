package de.awacademy.Shopmanager.link;


import de.awacademy.Shopmanager.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Link {

    @Id
    @GeneratedValue
    private long id;

    private String title;

    private String url;

    private int points;

    @ManyToOne
    private User user;

    public Link() {
        // Default constructor for JPA
    }

    public Link(String title, String url, User user) {
        this.title = title;
        this.url = url;
        this.points = 0;
        this.user = user;
    }

    public Link(String title, String url, int points, User user) {
        this.title = title;
        this.url = url;
        this.points = points;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public int getPoints() {
        return points;
    }

    public void upvote() {
        this.points++;
    }

    public void downvote() {
        this.points--;
    }

    public User getUser() {
        return user;
    }
}
